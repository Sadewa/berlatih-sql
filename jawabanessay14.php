<?php
#Membuat Tabel Customers
"
CREATE TABLE customers (
	id int NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	email VARCHAR(255),
	password VARCHAR (255),
	PRIMARY KEY (id)
);

"

#Membuat Tabel orders
"
CREATE TABLE orders (
	id int NOT NULL AUTO_INCREMENT,
	amount VARCHAR(255),
	customer_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (customer_id) REFERENCES customers(id)
);

"